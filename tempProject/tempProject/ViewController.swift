//
//  ViewController.swift
//  tempProject
//
//  Created by Jeriko on 2/14/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import DrawSig

class ViewController: UIViewController {
    
    @IBAction func nextPage(_ sender: UIButton) {
    }
    
    @IBOutlet weak var definedDrawingArea: DrawingView!
    
    @IBOutlet weak var drawingView02: DrawSignatureView!
    @IBOutlet weak var imageForSignature: UIImageView!
    var DrawSig = DrawSignatureAbstract()
    
    @IBAction func beginEditing(_ sender: UIButton) {
        DrawSig.beginEditingSignature(frame: imageForSignature.frame, viewController: self)
        drawingView02.beginEditingSignature()
    }
    
    @IBAction func cancelEditing(_ sender: UIButton) {
        DrawSig.endEditingSignature(viewController: self)
        drawingView02.endEditingSignature()
    }
    
    @IBAction func clearDrawings(_ sender: UIButton) {
        DrawSig.clearLines()
        drawingView02.clearLines()
    }
    
    @IBAction func saveImage(_ sender: UIButton) {
        //        DrawSig.controller.generateSignatureImage(capture: imageForSignature, viewController: self)
        DrawSig.generateImage()
        drawingView02.generateImage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidLayoutSubviews() {
        DrawSig.addSignatureScrollView(frame: imageForSignature.frame, image: #imageLiteral(resourceName: "receipt"), viewController: self)
        drawingView02.addSignatureScrollView()
        DrawSig.mainViewContainer = imageForSignature
        DrawSig.setDrawArea(area: definedDrawingArea, frame: imageForSignature.frame)
    }
}

