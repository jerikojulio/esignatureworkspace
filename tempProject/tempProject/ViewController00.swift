//
//  ViewController00.swift
//  tempProject
//
//  Created by Jeriko on 2/20/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import DrawSig

class ViewController00: UIViewController {
    
    
    @IBOutlet weak var mainContainerView: UIImageView!
    @IBOutlet weak var definedDrawingArea: DrawingView!
    
    var DrawSig = DrawSignatureAbstract()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
    override func viewDidLayoutSubviews() {
        DrawSig.addSignatureScrollView(frame: mainContainerView.frame, image: #imageLiteral(resourceName: "receipt"), viewController: self)
        DrawSig.mainViewContainer = mainContainerView
        DrawSig.setDrawArea(area: definedDrawingArea, frame: mainContainerView.frame)
    }
    
    @IBAction func finishEditing(_ sender: UIButton) {
        DrawSig.endEditingSignature(viewController: self)
    }
    @IBAction func beginEditing(_ sender: UIButton) {
        DrawSig.beginEditingSignature(frame: mainContainerView.frame, viewController: self)
    }
    
}
