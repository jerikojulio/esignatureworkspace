//
//  DrawSignatureView.swift
//  DrawSig
//
//  Created by Jeriko on 2/20/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

public class DrawSignatureView: UIView{
    public var backgroundImage = UIImageView()
    var drawView = DrawingView()
    var controller = DrawSigController()
    var drawScrollView = DrawScrollView()
    var didEditing = false
    var drawViewIsDefinedByUser = false
    var definedDrawingArea = DrawingView()
    var mainFrame = CGRect()
    
    @IBInspectable public var imageToDraw: UIImage = UIImage()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    public func addSignatureView(){
        backgroundImage = UIImageView(frame: self.bounds)
        backgroundImage.image = imageToDraw
        self.addSubview(backgroundImage)
        
        drawView = DrawingView(frame: self.frame)
        drawView.backgroundColor = UIColor.clear
        self.addSubview(drawView)
    }
    
    public func addSignatureScrollView(){
        drawScrollView = DrawScrollView(frame: self.bounds)
        drawScrollView.setUpImage(image: imageToDraw)
        self.addSubview(drawScrollView)
    }
    
    
    public func beginEditingSignature(){
        if !didEditing{
            drawView = DrawingView()
            drawView.backgroundColor = UIColor.clear
            let size = drawScrollView.imageView.image?.size
            
            if drawViewIsDefinedByUser{
                drawView.frame = drawScrollView.rescaleDrawingArea(area: definedDrawingArea, mainframe: mainFrame)
            } else {
                drawView.frame = drawScrollView.bounds
                drawView.frame.size = size!
            }
            
            drawScrollView.parentSubview.addSubview(drawView)
        }
        
        drawScrollView.lockZoom()
        didEditing = true
    }
    
    public func setDrawArea(area: DrawingView,frame: CGRect){
        definedDrawingArea = area
        mainFrame = frame
        drawViewIsDefinedByUser = true
    }
    
    public func endEditingSignature(){
        drawScrollView.unlockZoom()
        didEditing = false
    }
    
    public func clearLines(){
        drawView.clearLines()
    }
    
    public func generateImage(){
        controller.generateSignatureImageInView(capture: self.drawScrollView)
    }
    
    func multiDebug(){
        let drawView = UIImageView()
        drawScrollView.imageView.frame = drawScrollView.bounds
        drawScrollView.contentSize = drawScrollView.imageView.bounds.size
        drawScrollView.addSubview(drawView)
    }
}
