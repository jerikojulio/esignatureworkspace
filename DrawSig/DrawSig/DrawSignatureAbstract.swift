//
//  DrawSignature.swift
//  DrawSig
//
//  Created by Jeriko on 2/16/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import Foundation

public class DrawSignatureAbstract{
    public var mainViewContainer = UIView()
    public var backgroundImage = UIImageView()
    var drawView = DrawingView()
    var controller = DrawSigController()
    var drawScrollView = DrawScrollView()
    var didEditing = false
    var drawViewIsDefinedByUser = false
    var definedDrawingArea = DrawingView()
    var mainFrame = CGRect()
    
    public init(){
    }
    
    public func addSignatureView(frame: CGRect, image: UIImage, viewController: UIViewController){
        backgroundImage = UIImageView(frame: frame)
        backgroundImage.image = image
        viewController.view.addSubview(backgroundImage)
        
        drawView = DrawingView(frame: frame)
        drawView.backgroundColor = UIColor.clear
        viewController.view.addSubview(drawView)
    }
    
    public func addSignatureViewWithoutImage(){
        
    }
    
    public func addSignatureScrollView(frame: CGRect, image: UIImage, viewController: UIViewController){
        drawScrollView = DrawScrollView(frame: frame)
        drawScrollView.setUpImage(image: image)
        viewController.view.addSubview(drawScrollView)
    }
    
    
    
    public func beginEditingSignature(frame: CGRect, viewController: UIViewController){
        if !didEditing{
            drawView.backgroundColor = UIColor.clear
            drawView.layer.borderColor = UIColor.red.cgColor
            drawView.layer.borderWidth = 2
            let size = drawScrollView.imageView.image?.size
            
            if drawViewIsDefinedByUser{
                drawView.frame = drawScrollView.rescaleDrawingArea(area: definedDrawingArea, mainframe: mainFrame)
            } else {
                drawView.frame = drawScrollView.bounds
                drawView.frame.size = size!
            }
            drawScrollView.parentSubview.addSubview(drawView)
        }
        
        drawScrollView.setVisibleView()
        drawScrollView.lockZoom()
        didEditing = true
    }
    
    public func setDrawArea(area: DrawingView,frame: CGRect){
        definedDrawingArea = area
        mainFrame = frame
        drawViewIsDefinedByUser = true
    }
    
    public func endEditingSignature(viewController: UIViewController){
        drawScrollView.unlockZoom()
        didEditing = false
    }
    
    public func clearLines(){
        drawView.clearLines()
    }
    
    public func generateImage(){
        controller.generateSignatureImageInView(capture: self.drawScrollView)
    }
    
    func multiDebug(){
        let drawView = UIImageView()
        drawScrollView.imageView.frame = drawScrollView.bounds
        drawScrollView.contentSize = drawScrollView.imageView.bounds.size
        drawScrollView.addSubview(drawView)
    }
}
