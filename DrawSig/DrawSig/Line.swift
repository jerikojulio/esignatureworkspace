//
//  Line.swift
//  DrawSignature
//
//  Created by Jeriko on 2/14/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

class Line {
    var start: CGPoint
    var end: CGPoint
    
    init(start _start: CGPoint, end _end: CGPoint){
        start = _start
        end = _end
    }
    
}
