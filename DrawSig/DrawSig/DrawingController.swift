//
//  DrawSigController.swift
//  DrawSig
//
//  Created by Jeriko on 2/16/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import Foundation

public class DrawSigController {
    
    public func generateSignatureImage(capture: UIView, viewController: UIViewController){
        let size = capture.frame.size
        let posX = -capture.frame.minX
        let posY = -capture.frame.minY
        
        UIGraphicsBeginImageContext(size)
        
        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: posX, y: posY)
        
        viewController.view.layer.render(in: context!)
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(screenshot!, nil, nil, nil)
    }
    
    // MARK - Generate image inside parentSubview
    public func generateSignatureImageInView(capture: DrawScrollView) {
        let size = capture.imageView.bounds.size
        
        UIGraphicsBeginImageContext(size)
        
        let context = UIGraphicsGetCurrentContext()
        
        capture.parentSubview.layer.render(in: context!)
        
        let screenshot = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        UIImageWriteToSavedPhotosAlbum(screenshot!, nil, nil, nil)
    }
}
