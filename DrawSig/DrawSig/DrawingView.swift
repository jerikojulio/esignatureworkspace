//
//  DrawSignature.swift
//  DrawSignature
//
//  Created by Jeriko on 2/14/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit

public class DrawingView: UIView {
    
    private var lines: [Line]=[]
    private var lastPoint: CGPoint!
    public var controller = DrawSigController()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required public override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    
    override public func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let tempTouch = touches.first?.location(in: self) {
            lastPoint = tempTouch
        }
        self.setNeedsDisplay()
    }
    
    override public func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        var newPoint = CGPoint()
        if let tempTouch = touches.first?.location(in: self) {
            newPoint = tempTouch
        }
        lines.append(Line(start: lastPoint, end: newPoint))
        lastPoint = newPoint
        self.setNeedsDisplay()
    }
    
    override public func draw(_ rect: CGRect) {
        let context = UIGraphicsGetCurrentContext()
        context?.beginPath()
        for line in lines {
            context?.move(to: CGPoint(x: line.start.x, y: line.start.y))
            context?.addLine(to: CGPoint(x: line.end.x, y: line.end.y))
        }
        context?.setLineWidth(3)
        context?.setStrokeColor(cyan: 1, magenta: 1, yellow: 1, black: 1, alpha: 1)
        context?.strokePath()
    }
    
    public func clearLines(){
        lines.removeAll()
        self.setNeedsDisplay()
    }
    
}
