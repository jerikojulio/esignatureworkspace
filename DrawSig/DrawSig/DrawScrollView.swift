//
//  DrawScrollView.swift
//  DrawSig
//
//  Created by Jeriko on 2/16/17.
//  Copyright © 2017 Jeriko. All rights reserved.
//

import UIKit
import AVFoundation

public class DrawScrollView: UIScrollView, UIScrollViewDelegate {
    
    var imageView = UIImageView()
    var parentSubview = UIView()
    var drawingArea = CGRect()
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    required override public init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.red
    }
    
    public func setVisibleView(){
        self.setZoomScale(0, animated: true)
    }
    
    func setUpImage(image: UIImage){
        self.delegate = self
        imageView.image = image
        setUp()
    }
    
    func setUp() {
        let size = imageView.image?.size
        
        parentSubview.frame = self.bounds
        parentSubview.frame.size = size!
        
        imageView.frame = self.bounds
        imageView.frame.size = size!
        
        //        self.contentSize = size!
        rescaleImageToFitHolder()
        
//        self.contentSize = imageView.frame.size
        self.contentSize.height = imageView.frame.height
        self.contentSize.width = imageView.frame.width
        
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        self.addSubview(parentSubview)
        self.parentSubview.addSubview(imageView)
        setZoomProperties()
    }
    
    func rescaleDrawingArea(area: DrawingView, mainframe: CGRect) -> CGRect{
        var definedDrawingArea = area.frame
        
        let xOrigin = (definedDrawingArea.origin.x - mainframe.origin.x) / mainframe.width * self.imageView.frame.width
        let yOrigin = (definedDrawingArea.origin.y - mainframe.origin.y) / mainframe.height * self.imageView.frame.height
        
        let width = definedDrawingArea.width / self.frame.width * self.imageView.frame.width
        let height = definedDrawingArea.height / self.frame.height * self.imageView.frame.height
        
        definedDrawingArea = CGRect(x: xOrigin, y: yOrigin, width: width, height: height)
        drawingArea = definedDrawingArea
        return definedDrawingArea
    }
    
    func rescaleImageToFitHolder(){
        let imageViewBounds = imageView.bounds.size
        let mainScrollViewBounds = self.bounds.size
        
        let mainViewfactor = max(mainScrollViewBounds.height, mainScrollViewBounds.width)
        let imageViewFactor = max(imageViewBounds.height, imageViewBounds.width)
        
        let imageViewWidthFactor = imageViewBounds.width / imageViewFactor
        let imageViewHeightFactor = imageViewBounds.height / imageViewFactor
        
        let mainViewWidthFactor = mainScrollViewBounds.width / mainViewfactor
        let mainViewHeightFactor = mainScrollViewBounds.height / mainViewfactor
        
        var newHeight = CGFloat()
        var newWidth = CGFloat()
        
        if (mainViewWidthFactor / imageViewWidthFactor) == 1 {
            newHeight = mainViewWidthFactor * imageViewBounds.width
            newWidth = imageViewBounds.width
        }
        
        if (mainViewHeightFactor / imageViewHeightFactor) == 1 {
            newHeight = imageViewBounds.height
            newWidth = mainViewWidthFactor * imageViewBounds.height
        }
        
        if (mainViewHeightFactor / imageViewWidthFactor) == 1 {
            newHeight = imageViewBounds.height
            newWidth = imageViewBounds.width * imageViewHeightFactor * mainViewWidthFactor
        }
        
        if (mainViewWidthFactor / imageViewHeightFactor) == 1 {
            newHeight = imageViewBounds.height * imageViewWidthFactor * mainViewHeightFactor
            newWidth = imageViewBounds.width
        }
        
        imageView.frame.size.height = newHeight
        imageView.frame.size.width = newWidth
        parentSubview.frame = imageView.frame
    }
    
    func viewWillLayoutSubviews() {
        setZoomProperties()
    }
    
    public func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return parentSubview
    }
    
    func setZoomProperties() {
        let imageViewBounds = imageView.bounds.size
        let mainScrollViewBounds = self.bounds.size
        
        let widthScale = mainScrollViewBounds.width / imageViewBounds.width
        let heightScale = mainScrollViewBounds.height / imageViewBounds.height
        
        self.minimumZoomScale = max(widthScale,heightScale)
        self.maximumZoomScale = 0.5
        
        self.zoomScale = 0
    }
    
    func lockZoom(){
        self.pinchGestureRecognizer?.isEnabled = false
        self.panGestureRecognizer.isEnabled = false
    }
    
    func unlockZoom(){
        self.pinchGestureRecognizer?.isEnabled = true
        self.panGestureRecognizer.isEnabled = true
    }
}
